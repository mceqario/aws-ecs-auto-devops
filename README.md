### NodeJS App uisng AWS ECS Auto Deploy with Auto DevOps

This project is used in conjunction with AWS ECS Cluster.  Below are the following key AWS components (GitLab Environment Variables) necessary to run a successful pipeline.

* AUTO_DEVOPS_PLATFORM_TARGET = ECS

* AWS_ACCESS_KEY_ID = {AWS Access yey}

* AWS_SECRET_ACCESS_KEY = {AWS Secret Access key}

* AWS_REGION = {AWS cluster region}

* CI_AWS_ECS_Cluster [mce-ecs-ado-cluster](https://us-west-2.console.aws.amazon.com/ecs/home?region=us-west-2#/clusters/mce-ecs-ado-cluster/services)

* CI_AWS_ECS_SERVICE [mce-ecs-ado-service](https://us-west-2.console.aws.amazon.com/ecs/home?region=us-west-2#/clusters/mce-ecs-ado-cluster/services/mce-ecs-ado-service/details)

* CI_AWS_ECS_TASK_DEFINITION [mce-ecs-ado-td](https://us-west-2.console.aws.amazon.com/ecs/home?region=us-west-2#/taskDefinitions/mce-ecs-ado-td/4)



Currently the ingress to AWS ALB is not working.  In the interim you can view the container for changes by pulling the images and running the container on your laptop.  See steps below.

- Make changes in a feature branch

- Run pipeline for the feature branch

- Locate the latest image pushed in to GitLab container registry (Packages & Registsries | Container Registry) and copy the link

- Pull the latest image in the GitLab registry: 
docker pull registry.gitlab.com/mceqario/aws-ecs-auto-devops/1-feature-branch:latest

- Create a container from pulled image 
docker run --name aws-ecs-ado-demo{n} -p 8{n}:5000 -d {image ID from “Pull registry step above} where 'n' is the next image (if images were not cleaned up)

- In browser 'localhost:8{n}' to see changes

- Execute the container if needed to view container content_
docker exec -it d4f52da6e43843c127ba5cca46a9226b3d8981daa7207d1facf4889dc3ab3a92 /bin/bash

(cd to /app and then open the appropriate directory/file to show changes)

